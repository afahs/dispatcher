#!/usr/bin/python3

#This code is responsible for clearing, sorting and pickling the raw data from 
# Trento Grid
import numpy as np
import pandas as pd 
 
def is_int(val):
    if type(val) == int:
        return True
    else:
        if val.is_integer():
            return True
        else:
            return False


def LoadDataFrame(dir1,names):
	f = open(dir1, "r")
	data=[]
	for x in f:
		l=[]
		for a in x.replace("\n","").split("\t"):
			if a == '':
				#print("None detected!")
				l.append("N/A")
			else:
				if is_int(float(a)):
					l.append(int(a))
				else:
					l.append(float(a))
		data.append(l)
	df = pd.DataFrame(data,columns=names)
	return df

def FixTime(df):
	minT=df["Time"].min()
	for i in range(len(df.values)):
		df.at[i,"Time"]= (df.at[i,"Time"]-minT)/600000
	return df

def SaveDF(df,dir1):
	df.to_csv(dir1)

def PickleDF(df,dir1):
	df.to_pickle(dir1)

if __name__ == "__main__":
	names=["BS", "Time", "CC", "SMSIn","SMSOut", "CallOut", "CallIn", "Internet" ]
	date="2013-12-29"

	for i in range(31): 
		if i+1 < 10: 
			s="0"+str(i+1)
		else: 
			s=str(i+1)
		#date="2013-12-"+s
		df = LoadDataFrame("./Raw/sms-call-internet-tn-"+date+".txt",names)
		df = FixTime(df)
		print("finshed " +date)
		PickleDF(df,"./Pk/"+date+".pk")
		break

		

	 



		

		