#!/usr/bin/python3

import pandas as pd
import random 
import math
import com
from tqdm import trange
ip={
"Frigo1":"131.254.150.180",
"Frigo2":"131.254.150.181",
"Frigo3":"131.254.150.182",
"Frigo4":"131.254.150.183",
"Frigo5":"131.254.150.184",
"Node1":"131.254.150.185",
"Node2":"131.254.150.186",
"Node3":"131.254.150.187",
"Node4":"131.254.150.188",
"Node5":"131.254.150.189",
"Node6":"131.254.150.190",
"Node7":"131.254.150.191",
"Frigo1-1":"131.254.150.140",
"Frigo1-2":"131.254.150.141",
"Frigo1-3":"131.254.150.142",
"Frigo1-4":"131.254.150.143",
"Frigo1-5":"131.254.150.144",
"Frigo2-1":"131.254.150.145",
"Frigo2-2":"131.254.150.146",
"Frigo2-3":"131.254.150.147",
"Frigo2-4":"131.254.150.148",
"Frigo2-5":"131.254.150.149"
}


def GetNodes(mapNode): 
	return list(mapNode.keys())

def EstimateLatency(ID1,ID2): 
	return CalacDistance(ID1,ID2) 

def CalacDistance(ID1,ID2):
	x1,y1=GetCoordinates(ID1)
	x2,y2=GetCoordinates(ID2)
	Distance=math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))
	# Colocated nodes (different names but same location)
	if Distance==0:
		Distance=1 #Colocated nodes have a roundtrip latency of 1ms
	return Distance

def GetCoordinates(ID): 
	x=(ID-1)%117
	y=math.floor((ID-1)/117)
	return x,y

def GetDistanceMap(mapNode): 
	Nodes=GetNodes(mapNode)
	l=[]
	for node in Nodes:
		d={}
		for node1 in Nodes:
			if node==node1: 
				d[node1]=0
			else: 
				d[node1]=CalacDistance(int(mapNode[node]),int(mapNode[node1]))
		l.append(pd.Series(d,name=node))
	df= pd.DataFrame(l)
	return df

def GetLatencyMap(mapNode):
	Nodes=GetNodes(mapNode)
	l=[]
	for node in Nodes:
		d={}
		for node1 in Nodes:
			if node==node1: 
				d[node1]=0.2
			else: 
				d[node1]=EstimateLatency(int(mapNode[node]),int(mapNode[node1]))
		l.append(pd.Series(d,name=node))
	df= pd.DataFrame(l)
	return df

def Create(df):
	for row in df.iterrows():
		CreateFile(row)
def CreateFile(row):
	f = open("Latencies/"+row[0]+".sh", "w")
	Initiate(f,row[0])
	d=row[1].to_dict()
	i=2
	for k,v in d.items(): 
		if k==row[0]: 
			continue
		l=str(round(v/2,2))+"ms"
		AddRule(f,row[0],i,k,l)
		i+=1	
	f.close()

def Initiate(f,C): 
	f.write("#"+C+"\n\n")
	f.write("IF=eth0\n")
	f.write("tc qdisc add dev $IF root handle 1: htb\n")
	f.write("tc class add dev $IF parent 1: classid 1:1 htb rate 1000Mbps\n\n\n")

def AddRule(f,C,i,k,l):
	f.write("#"+k+"\n\n")
	f.write("tc class add dev $IF parent 1:1 classid 1:"+str(i)+" htb rate 1000Mbps\n")
	f.write("tc qdisc add dev $IF handle "+str(i)+": parent 1:"+str(i)+" netem delay "+l+"\n")
	f.write("tc filter add dev $IF pref "+str(i)+" protocol ip u32 match ip dst "+ip[k]+" flowid 1:"+str(i)+"\n\n")

def SendFiles(Nodes):
	t = trange(len(Nodes), desc='Bar desc', leave=True)
	for i in t:
		t.set_description("Sending File to "+Nodes[i])
		t.refresh()
		com.SendTCFile(Nodes[i])

def ClearLatencies(Nodes):
	t1 = trange(len(Nodes), desc='Bar desc', leave=True)
	for i in t1:
		t1.set_description("Clearing TC in  "+Nodes[i])
		t1.refresh()
		com.ClearTC(Nodes[i])

def RunLatencies(Nodes): 
	t2 = trange(len(Nodes), desc='Bar desc', leave=True)
	for i in t2:
		t2.set_description("Running TC in   "+Nodes[i])
		t2.refresh()
		com.RunTC(Nodes[i])

def ExecuteLatencies(Nodes): 
	#Send the files
	SendFiles(Nodes)
	print("")
	#Clear the previous tc
	ClearLatencies(Nodes)
	print("")
	#Run the new Tc files
	RunLatencies(Nodes)
	print("")

def main(mapNode): 
	#df=GetDistanceMap(mapNode)
	df=GetLatencyMap(mapNode)
	print(df)
	df.to_csv("delay.csv")
	Create(df)
	ExecuteLatencies(GetNodes(mapNode))

