#!/usr/bin/python3
import matplotlib.pyplot as plt
from matplotlib import animation
import math 
import numpy as np
import pandas as pd 
import organize 
import random 
import com
import argparse
import sort
import time
import pickle
import late
import animate as anime
import sys
IP={
"Frigo1":"131.254.150.180",
"Frigo2":"131.254.150.181",
"Frigo3":"131.254.150.182",
"Frigo4":"131.254.150.183",
"Frigo5":"131.254.150.184",
"Frigo1-1":"131.254.150.140",
"Frigo1-2":"131.254.150.141",
"Frigo1-3":"131.254.150.142",
"Frigo1-4":"131.254.150.143",
"Frigo1-5":"131.254.150.144",
"Frigo2-1":"131.254.150.145",
"Frigo2-2":"131.254.150.146",
"Frigo2-3":"131.254.150.147",
"Frigo2-4":"131.254.150.148",
"Frigo2-5":"131.254.150.149",
"Node1":"131.254.150.185",
"Node2":"131.254.150.186",
"Node3":"131.254.150.187",
"Node4":"131.254.150.188",
"Node5":"131.254.150.189",
"Node6":"131.254.150.190",
"Node7":"131.254.150.191"
}

def parseCliOptions():

		parser = argparse.ArgumentParser()

		parser.add_argument( '--Cycle',#imp
			dest       = 'Cycle',  
			type       = float,
			default    = 10,
			help       = 'cycle time in seconds',
		)

		parser.add_argument( '--Inactive',#imp
			dest       = 'Inactive',  
			type       = int,
			default    = 10,
			help       = '# of inactive nodes',
		)


		parser.add_argument( '--Date', #imp
			dest       = 'Date', 
			type       = str,
			default    = '2013-11-03',
			help       = 'The date of the used data',
		)

		parser.add_argument( '--Load', #imp
			dest       = 'Load', 
			type       = str,
			default    = 'Internet',
			help       = 'The tested type of load',
		)

		parser.add_argument( '--Random', #imp
			dest       = 'Random', 
			action	   ='store_true',
			help       = 'The BS are selected randomly',
		)

		parser.add_argument( '--Animate', #imp
			dest       = 'Animate', 
			action     ='store_true',
			help       = 'Animate the Load distribution',

		)

		parser.add_argument( '--Change', #imp
			dest       = 'Change', 
			action     ='store_true',
			help       = 'Get new Load distribution',

		)
		parser.add_argument( '--Svc', #imp
			dest       = 'Svc', 
			type       = str,
			default    = "0.0.0.0",
			help       = 'The tested type of load',
		)


		options        = parser.parse_args()
		return options.__dict__

def PrintRow(Row):
	s=""
	for item in Row: 
		s+=str(item)+"\t"
	print(s)

def CountDown(x,T): 
	for remaining in range(int(x), 0, -1):
		sys.stdout.write("\r")
		sys.stdout.write("{:2d} seconds remaining. Total={:4d}  ".format(remaining,T))
		sys.stdout.flush()
		time.sleep(1)

def Distribute(dfhour,cycle,mapNode,Animate,Svc):
	newdf=pd.DataFrame(columns=["Time"]+ list(mapNode.keys()) )
	newdf.set_index('Time', inplace=True)
	newdf.to_pickle("./temp.pk")
	if Animate: 
		com.RunGraphs()
		time.sleep(2)
	#dfhour.set_index('Time', inplace=True)
	PrintRow(["Time"]+ list(mapNode.keys()))
	start=time.time()
	TC=2*60
	for i, row in dfhour.iterrows():
		if i!=12: 
			continue 
		start=time.time()
		print("\nCycle "+str(i)) 
		print("______________________________________________________________________")
		print("")
		PrintRow(list(row))
		T=sum(list(row))-i
		print("Total:", T)
		newdf.loc[i]=row
		newdf.to_pickle("./temp.pk")
		for j in range(newdf.loc[i].max()): 
			for node in list(newdf.columns):
				if newdf.at[i,node]>j:
					com.SendReq(node,Svc)
			time.sleep(.8)
		#time.sleep(cycle)
		print(start-time.time())
		print(start-time.time()+TC)
		CountDown(start-time.time()+TC,T)
		#input("press to continue")

def Distribute_Num(dfhour,cycle,mapNode,Animate,Svc):
	newdf=pd.DataFrame(columns=["Time"]+ list(mapNode.keys()) )
	newdf.set_index('Time', inplace=True)
	newdf.to_pickle("./temp.pk")
	if Animate: 
		com.RunGraphs()
		time.sleep(2)
	#dfhour.set_index('Time', inplace=True)
	PrintRow(["Time"]+ list(mapNode.keys()))
	start=time.time()
	TC=2*60
	for i, row in dfhour.iterrows():
		if i!=12: 
			continue 
		start=time.time()
		print("\nCycle "+str(i)) 
		print("______________________________________________________________________")
		print("")
		PrintRow(list(row))
		T=sum(list(row))-i
		print("Total:", T)
		newdf.loc[i]=row
		newdf.to_pickle("./temp.pk")
		for node in list(newdf.columns):
			#if newdf.at[i,node]>j:
			com.SendReqNum(node,Svc,newdf.at[i,node])
		time.sleep(.8)
		#time.sleep(cycle)
		print(start-time.time())
		print(start-time.time()+TC)
		CountDown(start-time.time()+TC,T)
		#input("press to continue")

def GetNodes():
	return list(IP.keys())

def GetlistInactive(Nodes,n): 
	assert len(Nodes)>n, "impossible value of n"
	Inactive=[]
	for i in range(n): 
		node=random.choice(list(set(Nodes) - set(Inactive)))
		Inactive.append(node)
	assert len(Inactive) == len(set(Inactive)),"Repetition in the inactive list"
	return Inactive

if __name__ == "__main__":
	options = parseCliOptions()
	date=options["Date"]
	load=options["Load"]
	cycle=options["Cycle"]
	animate=options["Animate"]
	svc=options["Svc"]
	Change=options["Change"]
	n=options["Inactive"]
	if Change: 
		df=sort.LoadPickle("./Pk/"+date+".pk")
		dfhour,dfmin,mapNode=sort.GetLoad(df,GetNodes(),7,load) #Number of spare nodes is hardcoded to 7
		Nodes=[]
		for k,v in mapNode.items(): 
			Nodes.append(k)
		dfhour.to_pickle("./dfhour.pk")
		dfmin.to_pickle("./dfmin.pk")
		Inactive=GetlistInactive(Nodes,n)
		print(Inactive.sort)
		pickle_out = open("Inactive.pk","wb")
		pickle.dump(Inactive, pickle_out)
		pickle_out.close()
		for node in Inactive: 
			dfhour.loc[:,node]=0
		late.main(mapNode)
		anime.MapNodes(mapNode)
		pickle_out = open("mapNode.pk","wb")
		pickle.dump(mapNode, pickle_out)
		pickle_out.close()
		input("Press Enter to continue...")
	else:
		mapNode=pickle.load( open( "mapNode.pk", "rb" ) )
		# df=sort.LoadPickle("./Pk/"+date+".pk")
		# # dfhour,dfmin,mapNode=sort.GetLoadMapped2(df,load,mapNode)
		# # dfhour.to_pickle("./dfhour.pk")
		# # print(dfhour)
		Inactive=pickle.load( open( "Inactive.pk", "rb" ) )

		dfhour=pd.read_pickle("./dfhour.pk")

		# Inactive=GetlistInactive(GetNodes(),4)
		# # # Inactive.append("Node6")
		# Inactive.append("Frigo1")
		# pickle_out = open("Inactive.pk","wb")
		# pickle.dump(Inactive, pickle_out)
		# pickle_out.close()
		# print(Inactive)
		for node in Inactive: 
			dfhour.loc[:,node]=0
		print(dfhour)
		for index,row in dfhour.iterrows(): 
			print("Sum",sum(list(row))-index)
		input("Press Enter to continue...")
	Distribute_Num(dfhour,cycle,mapNode,animate,svc)
