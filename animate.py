#!/usr/bin/python3
import sys
sys.path.append('./geojson')

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pandas as pd
import time
import organize
import numpy as np
import pickle
import json as js
import libraries as li

def animate(i):
	df=pd.read_pickle("temp.pk")
	pickle_in = open("mapNode.pk","rb")
	mapNode = pickle.load(pickle_in)
	Nodes=list(df.columns)
	for i in range(len(Nodes)):
		#print(Nodes[i])
		ax1[i].clear()
		ax1[i].set_xlim([0, 24])
		ax1[i].set_ylim([0, 30])
		ax1[i].set_title(Nodes[i]+":"+mapNode[Nodes[i]])
		ax1[i].set_ylabel('# Requests')
		if i == len(Nodes)-1:
			ax1[i].set_xlabel('Time (Hour)')
		ax1[i].plot(df.index,df[Nodes[i]],marker='o')

		if i == len(Nodes)-1:
			ax1[i].set_xlabel('Time (Sec)')
	#ax1[0].clear()
	# ax1[1].clear()
	# ax1[0].set_xlim([0, 24])
	# ax1[0].set_ylim([0, 30])
	# ax1[1].set_xlim([0, 24])
	# ax1[1].set_ylim([0, 30])
	# #ax1.plot(xnew,power_smooth)
	# ax1[0].plot(df.index,df["Frigo1"],marker='o',)
	# ax1[1].plot(df.index,df["Frigo2"],marker='o',)


def MapNodes(mapNode):
	IDS=[v for k,v in mapNode.items()] 
	data=li.LoadGeoJson("./trentino-grid.geojson")
	Cells=data['features']
	newcells=[]
	for cell in Cells:
		ID=cell["properties"]["cellId"]
		if str(ID) in IDS:
			cell["properties"]["fill"]="#800026"		
		else: 
			cell["properties"]["fill"]="#ffffcc"
		newcells.append(cell)
	data['features']=newcells
	li.CreateHtmlSelectedNodes(data,opacity=0.4)
	li.DumpGeoJson("SelectedNodes.geojson",data)

if __name__ == "__main__":
	df=pd.read_pickle("temp.pk")
	fig, ax1 = plt.subplots(len(df.columns))
	ani = animation.FuncAnimation(fig, animate, interval=300)
	plt.tight_layout()
	plt.show()