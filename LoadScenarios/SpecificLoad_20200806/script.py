#!/usr/bin/python3

####STD LIBS#######
import pandas as pd
###################


## Transform the original dfhour file into a csv file
#pkData=pd.read_pickle("dfhour.pk")
#pkData.to_csv("load_df.csv")

## Transform the modified load_df csv file into a pk file

csvData=pd.read_csv("load_df.csv")
csvData.to_pickle("dfhour.pk")
