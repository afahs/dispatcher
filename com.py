#!/usr/bin/python3
import subprocess as commands
import logging


IP={
"Frigo1":"131.254.150.180",
"Frigo2":"131.254.150.181",
"Frigo3":"131.254.150.182",
"Frigo4":"131.254.150.183",
"Frigo5":"131.254.150.184",
"Node1":"131.254.150.185",
"Node2":"131.254.150.186",
"Node3":"131.254.150.187",
"Node4":"131.254.150.188",
"Node5":"131.254.150.189",
"Node6":"131.254.150.190",
"Node7":"131.254.150.191",
"Frigo1-1":"131.254.150.140",
"Frigo1-2":"131.254.150.141",
"Frigo1-3":"131.254.150.142",
"Frigo1-4":"131.254.150.143",
"Frigo1-5":"131.254.150.144",
"Frigo2-1":"131.254.150.145",
"Frigo2-2":"131.254.150.146",
"Frigo2-3":"131.254.150.147",
"Frigo2-4":"131.254.150.148",
"Frigo2-5":"131.254.150.149"}



def SendReq(Node,Svcip):
	ip=IP[Node]
	tup=None
	if "Frigo" in Node:
		logging.info("Running: ssh frigo@"+ip+" curl -s "+Svcip)
		print("Running: ssh frigo@"+ip+" curl "+Svcip)
		#tup = commands.getstatusoutput("ssh frigo@"+ip+" curl -s "+Svcip)
		commands.Popen(["ssh","frigo@"+ip,"curl","-s",Svcip])
	if "Node" in Node: 
		logging.info("Running: ssh ali@"+ip+" curl -s "+Svcip)
		print("Running: ssh ali@"+ip+" curl "+Svcip)
		#tup = commands.getstatusoutput("ssh ali@"+ip+" curl -s "+Svcip)
		commands.Popen(["ssh","ali@"+ip,"curl","-s",Svcip])
	return tup

def SendReqNum(Node,Svcip,Num):
	ip=IP[Node]
	tup=None
	if "Frigo" in Node:
		logging.info("Running: ssh frigo@"+ip+" curl -s "+Svcip)
		print("Running: ssh frigo@"+ip+" ./call.sh "+Svcip+" "+ str(Num))
		commands.Popen(["ssh","frigo@"+ip,"./call.sh",Svcip,str(Num)])
	if "Node" in Node: 
		logging.info("Running: ssh ali@"+ip+" curl -s "+Svcip)
		print("Running: ssh ali@"+ip+" ./call.sh "+Svcip+" "+ str(Num))
		#tup = commands.getstatusoutput("ssh ali@"+ip+" curl -s "+Svcip)
		commands.Popen(["ssh","ali@"+ip,"./call.sh",Svcip,str(Num)])
	return tup


def RunGraphs(): 
	logging.info("Running: ./animate.py")
	tup=commands.Popen("./animate.py")

def SendTCFile(node): 
	ip=IP[node]
	if "Frigo" in node:
		logging.info("scp ./Latencies/"+node+".sh frigo@"+ip+":/home/frigo")
		#print("Running: scp ./Latencies/"+node+".sh  frigo@"+ip+":/home/frigo")
		tup = commands.getstatusoutput("scp ./Latencies/"+node+".sh  frigo@"+ip+":/home/frigo")
	if "Node" in node: 
		logging.info("scp ./Latencies/"+node+".sh  ali@"+ip+":/home/ali")
		#print("Running: scp ./Latencies/"+node+".sh  ali@"+ip+":/home/ali")
		tup = commands.getstatusoutput("scp ./Latencies/"+node+".sh  ali@"+ip+":/home/ali")

def ClearTC(node):
	ip=IP[node]	
	#tc qdisc del dev eth0 root"
	logging.info("ssh root@"+ip+" tc qdisc del dev eth0 root")
	#print("Running: ssh root@"+ip+" tc qdisc del dev eth0 root")
	tup = commands.getstatusoutput("ssh root@"+ip+" \"tc qdisc del dev eth0 root\"")	

def RunTC(node):
	ip=IP[node]
	if "Frigo" in node:
		logging.info("ssh root@"+ip+" /home/frigo/"+node+".sh")
		#print("Running: ssh root@"+ip+" /home/frigo/"+node+".sh")
		tup = commands.getstatusoutput("ssh root@"+ip+" /home/frigo/"+node+".sh")
	if "Node" in node: 
		logging.info("ssh root@"+ip+" /home/ali/"+node+".sh")
		#print("Running: ssh root@"+ip+" /home/ali/"+node+".sh")
		tup = commands.getstatusoutput("ssh root@"+ip+" /home/ali/"+node+".sh")