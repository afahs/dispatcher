#!/usr/bin/python3
import json as js
import geopandas as gpd
import folium
import os

def LoadGeoJson(path): 
	with open(path) as f:
		data = js.load(f)
	return data


def CreateHtml(date,opacity):
	data=LoadGeoJson("./HeatMap_Trento/Trento_"+date+".geojson")
	Hmap = folium.Map(location=[46.066666, 11.116667], zoom_start=12)
	folium.GeoJson(data, name="Trento").add_to(Hmap)
	folium.GeoJson(data, name="Trento",   style_function=lambda x: {
        'color' : x['properties']['fill'],
        'opacity': opacity,
        'fillOpacity': opacity,
        'fillColor' : x['properties']['fill'],
        }).add_to(Hmap)

	Hmap.save("./Html/"+date+".html")

if __name__ == "__main__":
	CreateHtml("2013-12-15")