#!/usr/bin/python3
import glob
from PIL import Image
from selenium import webdriver 
import os

# get a list of all the files to open

def getaphoto(date):
	file_location="/home/ali-jawad/Desktop/data1/geojson/Html"
	glob_folder = os.path.join(file_location, '*.html')

	html_file_list = glob.glob(glob_folder)

	for html_file in html_file_list:
		if date not in html_file: 
			continue
		#get the name into the right format
		temp_name = "file://" + html_file
		# open in webpage
		driver = webdriver.Chrome()
		driver.get(temp_name)      
		driver.save_screenshot("./Images/"+date+".png")
		driver.quit()

		# crop as required
		img = Image.open("./Images/"+date+".png",mode='r')
		width, height = img.size
		print(img.size)
		hcrop=800
		vcrop=100

		box = (int(hcrop/2), int(vcrop/2), width - int(hcrop/2), height -int(vcrop/2)-50)
		area = img.crop(box)
		area.save("./Images/"+date+".png", 'png')

if __name__ == "__main__":
	getaphoto("2013-12-15")
