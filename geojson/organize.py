#!/usr/bin/python3
import numpy as np
import pandas as pd 

def is_int(val):
    if type(val) == int:
        return True
    else:
        if val.is_integer():
            return True
        else:
            return False


def LoadDataFrame(dir1,names):
	f = open(dir1, "r")
	data=[]
	for x in f:
		l=[]
		for a in x.replace("\n","").split("\t"):
			if a == '':
				#print("None detected!")
				l.append("N/A")
			else:
				if is_int(float(a)):
					l.append(int(a))
				else:
					l.append(float(a))
		data.append(l)
	df = pd.DataFrame(data,columns=names)
	return df

def FixTime(df):
	minT=df["Time"].min()
	for i in range(len(df.values)):
		df.at[i,"Time"]= (df.at[i,"Time"]-minT)/600000
	return df

def SaveDF(df,dir1):
	df.to_csv(dir1)

def PickleDF(df,dir1):
	df.to_pickle(dir1)
if __name__ == "__main__":
	names=["BS", "Time", "CC", "SMSIn","SMSOut", "CallOut", "CallIn", "Internet" ]
	date="2013-11-01"


	df = LoadDataFrame("./Raw/sms-call-internet-tn-"+date+".txt",names)
	df = FixTime(df)
	print(df)
	SaveDF(df,"./Csv/"+date+".csv")
	PickleDF(df,"./Pk/"+date+".pk")
	#print(df["Time"].min())
	#for  t in df["Time"].values:
		#print(t)
	# for index,row in df.iterrows():
	# 	if is_int(row['Time']/60000)==False:
	# 		print(row['Time']/60000,is_int(row['Time']/60000))
		

	 



		

		