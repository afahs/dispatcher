#!/usr/bin/python3 
import json as js
import pprint
import sort
import pandas as pd
from math import floor

def LoadGeoJson(path): 
	with open(path) as f:
		data = js.load(f)
	return data

def DumpGeoJson(path,geodict):
	with open(path, 'w') as json_file:
		js.dump(geodict, json_file)

def PrepareDf(date,load):
	df=sort.LoadPickle("../Pk/"+date+".pk")
	newdf=sort.Requests(df,load)
	newdf.set_index('BS', inplace=True)
	newdf.to_pickle("../RBS/"+date+"_RBS.pk")
	print(date)

def LoadDF(date): 
	df=sort.LoadPickle("../RBS/"+date+"_RBS.pk")
	return df

def GetColor(Req): 
	map={1:"#ffffcc",
	2:"#ffeda0",
	3:"#fed976",
	4:"#feb24c",
	5:"#fd8d3c",
	6:"#fc4e2a",
	7:"#e31a1c",
	8:"#bd0026",
	9:"#800026"}
	# map={1:"green",
	# 2:"green",
	# 3:"green",
	# 4:"green",
	# 5:"red",
	# 6:"red",
	# 7:"red",
	# 8:"red",
	# 9:"red"}
	if Req>449 : 
		return map[9]
	else: 
		return map[floor(Req/50)+1]

def AssignColors(data,df): 
	Cells=data['features']
	# newcells=[]
	for cell in Cells:
		ID=cell["properties"]["cellId"]
		if ID in df.index: 
			Req=df.at[ID,"#Req"]
		else: 
			Req=0
		cell["properties"]["#Req"]=str(Req)
		cell["properties"]["fill"]=GetColor(Req)
	data['features']=Cells
	return data 

def HighlightTrento(x=51,y=44): 
	IDList=[]
	StartX=x-9
	EndX=x+10
	StartY=y-9
	EndY=y+10
	for i in range(StartX,EndX+1): 
		for j in range(StartY,EndY+1):
			IDList.append(117*j+i+1)
	return(IDList)

def AssignColors(data,df): 
	Cells=data['features']
	# newcells=[]
	for cell in Cells:
		ID=cell["properties"]["cellId"]
		if ID in df.index: 
			Req=df.at[ID,"#Req"]
		else: 
			Req=0
		cell["properties"]["#Req"]=str(Req)
		cell["properties"]["fill"]=GetColor(Req)
		cell["properties"]["color"]=GetColor(Req)
	data['features']=Cells
	return data 

def CreateHeatMapTrento(date,x=51,y=44):
	df=sort.LoadPickle("../RBS/"+date+"_RBS.pk")
	data=LoadGeoJson("./trentino-grid.geojson")
	IDList=HighlightTrento(x=51,y=44)
	newcells=[]
	Cells=data['features']
	for cell in Cells: 
		if cell["properties"]["cellId"] in IDList: 
			newcells.append(cell)
	data['features']=newcells
	data=AssignColors(data,df)
	DumpGeoJson("./HeatMap_Trento/Trento_"+date+".geojson",data)

def CreateHeatMap(date): 
	df=sort.LoadPickle("../RBS/"+date+"_RBS.pk")
	data=LoadGeoJson("./trentino-grid.geojson")
	data=AssignColors(data,df)
	DumpGeoJson("./HeatMap/"+date+".geojson",data)

if __name__ == "__main__":
	date="2014-01-01"
	for i in [1,2]:
		m="1"+str(i)+"-" 
		for j in range(1,32): 
			if j==31 and i==1: 
				continue
			if j<10: 
				d="0"+str(j)
			else: 
				d=str(j)
			date="2013-"+m+d
			print(date)
			CreateHeatMapTrento(date,x=51,y=44)
	#IDList=HighlightTrento(data,x=51,y=44)
	#CreateHeatMap(date)




