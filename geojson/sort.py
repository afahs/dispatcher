#!/usr/bin/python3
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd 
import organize 
import random 
from math import floor

def LoadPickle(dir1): 
	return pd.read_pickle(dir1)

def ChooseBS(df,bs,load):
	resdf=df[df["BS"].isin([bs])]
	resdf=resdf[["BS", "Time", load]]
	return resdf

def BinReq(df):
	for i in range(len(df.values)):
		if df.iat[i,2]!= "N/A":
			df.iat[i,2]=1
		else : df.iat[i,2]=0
	return df

def CountDfMin(df):
	newdf=pd.DataFrame(columns=["Time","freq"])
	Time=0
	summ=0
	for i in range(144):
		summ=0
		for j in range(len(df.values)): 
			if df.iat[j,1]==i:
				summ+=1
		newdf.loc[i]=[i,summ]
	return newdf

def CountDfHour(df):
	Time=0
	summ=0
	hourdf=pd.DataFrame(columns=["Time","freq"])
	for i in range(144):
		if (i+1)%6==0:
			summ+=df.at[i,"freq"] 
			hourdf.loc[Time]=[Time,summ]
			summ=0
			Time+=1
		else:
			summ+=df.at[i,"freq"]
	return hourdf

def CountDf(df,bs,load): # the function that returns the number of requests 
# each hour and each 10 mins for a single bas station
	resdf=ChooseBS(df,bs,load)
	resdf=BinReq(resdf)	
	resdf=resdf[resdf[load].isin([1])]
	mindf=CountDfMin(resdf)
	hourdf=CountDfHour(mindf)
	return mindf,hourdf

def OrganizeDf(df,load): 
	newdf=df[["BS",load]]
	#newdf=newdf[newdf["BS"].isin(range(400))]
	newdf=newdf[~newdf[load].isin(["N/A"])]
	return newdf

def HighlightTrento(x=51,y=44): 
	IDList=[]
	StartX=x-9
	EndX=x+10
	StartY=y-9
	EndY=y+10
	for i in range(StartX,EndX+1): 
		for j in range(StartY,EndY+1):
			IDList.append(117*j+i+1)
	return(IDList)

def OrganizeDf2(df,load): 
	IDList=HighlightTrento(x=51,y=44)
	newdf=df[["BS","Time",load]]
	#newdf=newdf[newdf["BS"].isin(range(400))]
	newdf=newdf[~newdf[load].isin(["N/A"])]
	newdf=newdf[newdf["BS"].isin(IDList)]
	return newdf

def ReqPerBs(df):
	# a function that will return the number of requests received by the bs 
	# in a full day
	l={}
	for i in range(len(df.values)):
		if df.iat[i,0] not in list(l.keys()): 
			l[df.iat[i,0]]=1
		else: 
			l[df.iat[i,0]]+=1 
	newdf = pd.DataFrame(l.items(),columns=["BS","#Req"])
	newdf=newdf.sort_values(by=["BS"])
	s=pd.Series(range(len(newdf.values)))
	newdf=newdf.set_index(s)
	return newdf 


def GetRandomBS(df,n): 
	l=df.BS.unique().tolist()
	RandomBs=random.choices(l, k=n)
	print(RandomBs)
	return RandomBs

def CreateMap(Nodes,LBS): 
	mapNode={}
	i=0
	for n in Nodes:
		mapNode[n]=str(LBS[i])
		i+=1
	return mapNode

def GetLoad(df,Nodes,load): 
	RandomBs=GetRandomBS(df,len(Nodes))
	mapNode=CreateMap(Nodes,RandomBs)
	names=["Time"]
	for n in Nodes: 
		names.append(str(n))
	newdf = pd.DataFrame(columns=names)
	newdfmin = pd.DataFrame(columns=names)
	dfsmin=[]
	dfshour=[]
	for bs in RandomBs:
		mindf,hourdf=CountDf(df,bs,load)
		dfsmin.append(mindf)
		dfshour.append(hourdf)
	for i in range(len(dfshour[0].values)):
		temp=[i]
		for j in range(len(RandomBs)):
			temp.append(dfshour[j].at[i,"freq"])
		newdf.loc[i]=temp

	for i in range(len(dfsmin[0].values)):
		temp=[i]
		for j in range(len(RandomBs)):
			temp.append(dfsmin[j].at[i,"freq"])
		newdfmin.loc[i]=temp

	return newdf,newdfmin,mapNode

def Requests(df,load): 
	newdf=OrganizeDf(df,load)
	newdf=ReqPerBs(newdf)
	organize.PickleDF(newdf,"reqperBS.pk")
	return newdf

def ReqPerBsPerHour(df):
	l={}
	names = ["BS"]
	for i in range(0,24): 
		names.append(str(i))

	for i in range(len(df.values)):
		# if df.iat[i,0]!= 6588: 
		# 	continue
		if df.iat[i,0] not in list(l.keys()): 
			l[df.iat[i,0]]=[0 for i in range(0,24)]
			l[df.iat[i,0]][floor(df.iat[i,1]/6)]+=1
		else: 
			l[df.iat[i,0]][floor(df.iat[i,1]/6)]+=1 
	#print(l)
	i=0
	temp=[]
	for k,v in l.items():
		temp1=[k]
		for j in v:
			temp1.append(j)
		temp.append(temp1) 
	
	newdf=pd.DataFrame(temp,columns=names)
	newdf.set_index('BS', inplace=True)
	#newdf = pd.DataFrame(l.items(),columns=names)
	return newdf

def Requests2(load,date): 
	df=LoadPickle("../Pk/"+date+".pk")
	newdf=OrganizeDf2(df,load)
	newdf=ReqPerBsPerHour(newdf)
	newdf.to_pickle("../RBSHour_trento/RBSHour_Trento_"+date+".pk")

if __name__ == "__main__":
	
	load="Internet"
	date="2013-12-01"
	for i in range(1,3):
		m="1"+str(i)+"-" 
		for j in range(1,32): 
			if i==1 and j==31: 
				continue
			if j<10: 
				d="0"+str(j)
			else: 
				d=str(j)
			date="2013-"+m+d
			print("starting "+date)
			#Requests2(load,date)
	date="2014-01-01"
	Requests2(load,date)

